***Settings***
Documentation  Insly Demo Instance Test Cases
Library  SeleniumLibrary
Library  OperatingSystem
Library  String
Library  Screenshot
Library  Process


***Variables***
${IMAGE_COMPARATOR_COMMAND}   /usr/local/Cellar/imagemagick/7.0.8-39/bin/convert __REFERENCE__ __TEST__ -metric RMSE -compare -format  "%[distortion]" info:
${URL}  https://signup.int.staging.insly.training
${Browser}  safari
${Work_Email_Manually_Filled}
${Password}
${Broker_Tag_Automatically_Filled}
${Broker_Tag}  broker_tag
${Company_Address}  broker_address_country
${Company_Profile}  prop_company_profile
${Company_Employees}   prop_company_no_employees
${Company_Description}   prop_company_person_description
${Broker_Name}  broker_name 
${Broker_Admin_Name}  broker_admin_name
${Broker_Admin_Email}  broker_admin_email
${Secure_Password_Link}  //*[contains(text(), 'suggest a secure password')]
${Broker_Admin_Phone}   broker_admin_phone
${OK_Button}   //*[contains(text(), 'OK')]  
${Store_Password}  //*[@id='insly_alert']/b
${Repeat_Password_Field}  broker_person_password_repeat
${Terms_And_Conditions_Link}  //*[contains(text(), 'terms and conditions')]
${Privacy_Policy_Link}  //*[contains(text(), 'privacy policy')] 
${Submit_Button}   submit_save
${Login_Username}   login_username
${Login_Password}  login_password
${Login_Submit}   login_submit
${Agree_Button}  css=[class="primary"]
${Close_Dialog_Box}  //*[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"]//span[@class="icon-close"]
${Terms_And_Conditions_Check_Box}  //*[@id="field_terms"]//label[1]/span
${Privacy_Policy_Check_Box}  //*[@id="field_terms"]//label[2]/span
${Personal_Data_Storage_Check_Box}  //*[@id="field_terms"]//label[3]/span


***Keywords***
Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})
    
Compare Images
   [Arguments]      ${Reference_Image_Path}    ${Test_Image_Path}    ${Allowed_Threshold}
   ${TEMP}=         Replace String     ${IMAGE_COMPARATOR_COMMAND}    __REFERENCE__     ${Reference_Image_Path}
   ${COMMAND}=      Replace String     ${TEMP}    __TEST__     ${Test_Image_Path}
   Log              Executing: ${COMMAND}
   ${RC}            ${OUTPUT}=     Run And Return Rc And Output     ${COMMAND}
   Log              Return Code: ${RC}
   Log              Return Output: ${OUTPUT}
   ${RESULT}        Evaluate    ${OUTPUT} < ${Allowed_Threshold}
   Should be True   ${RESULT}

Launch Insly Demo Instance
   Open Browser  ${URL}  ${Browser}
   Maximize Browser Window 
   
Insly Sign Up
    Wait Until Page Contains  Sign up and start using  60s
    Capture Page Screenshot  Insly_Test.png
    Run Keyword And continue On Failure  Compare Images  ./Insly_Reference.png   ./Insly_Test.png   0.2
    
    
Enter Company Details
    ${Company_Name}  Generate Random String  5   [LETTERS]
    Input Text  ${Broker_Name}   ${Company_Name}
    Select From List   ${Company_Address}  Estonia
    Input Text  ${Broker_Tag}  amrin
    ${Broker_Tag_Manually_Filled}  Get Value  ${Broker_Tag}
    Select From List   ${Company_Profile}   Insurance Agency
    Select From List   ${Company_Employees}  1-5
    Select From List   ${Company_Description}  I am the CEO of the company
    ${Company_Name_Manually_Filled}  Get Value  ${Broker_Name} 
    ${Company_Address_Manually_Selected}  Get Value  ${Company_Address}
    ${Company_Profile_Manually_Selected}  Get Value  ${Company_Profile} 
    ${Company_Employees_Manually_Selected}  Get Value  ${Company_Employees}
    ${Company_Description_Manually_Selected}  Get Value  ${Company_Description}
    Should Not Be Empty  ${Company_Name_Manually_Filled}  
    Sleep  2s
    ${Broker_Tag_Automatically_Filled}  Get Value  ${Broker_Tag}
    Should Not Be Empty  ${Company_Profile_Manually_Selected}  
    Should Not Be Empty  ${Company_Address_Manually_Selected} 
    Should Not Be Empty  ${Company_Employees_Manually_Selected} 
    Should Not Be Empty  ${Company_Description_Manually_Selected} 
    ${Company_Name}  Convert To Lower Case  ${Company_Name}
    ${Broker_Tag_Automatically_Filled}  Convert To Lower Case  ${Broker_Tag_Automatically_Filled}
    Set Global variable  ${Broker_Tag_Automatically_Filled}
    Should Be Equal  ${Company_Name}  ${Broker_Tag_Automatically_Filled}  
    
Enter Administrator Account Details
    ${Random_Email}  Generate Random String  5   [LETTERS]
    Input Text  ${Broker_Admin_Email}  ${Random_Email}@ee.com
    Input Text  ${Broker_Admin_Name}  Abdul Amrin Sda
    Click Element  ${Secure_Password_Link}   
    Wait Until Page Contains Element  ${Store_Password} 
    ${Password}  Get Text   ${Store_Password} 
    Set Global variable  ${Password}
    Click Button  ${OK_Button}   
    Input Text  ${Broker_Admin_Phone}  12345678
    ${Work_Email_Manually_Filled}  Get Value  ${Broker_Admin_Email}
    Set Global variable  ${Work_Email_Manually_Filled}
    ${Admin_Name_Manually_Filled}  Get Value  ${Broker_Admin_Name}
    ${Password_Repeat}  Get Value  ${Repeat_Password_Field}
    ${Admin_Phone_Manually_Filled}  Get Value  ${Broker_Admin_Phone}
    Should Not Be Empty  ${Work_Email_Manually_Filled}
    Should Not Be Empty  ${Admin_Name_Manually_Filled}
    Should Not Be Empty  ${Password_Repeat}
    Should Not Be Empty  ${Admin_Phone_Manually_Filled} 
           
Tick All Terms and Conditions  
    Click Element  ${Terms_And_Conditions_Link}  
    Sleep  5s
    Click Button  ${Agree_Button}   
    Click Element  ${Privacy_Policy_Link}  
    Sleep  6s
    Execute Javascript    window.document.evaluate("//*[contains(text(), 'Revision: 1.20180525')] ", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
    Sleep  2s
    Click Element  ${Close_Dialog_Box}
    Click Element  ${Terms_And_Conditions_Check_Box}
    Click Element  ${Privacy_Policy_Check_Box}
    Click Element  ${Personal_Data_Storage_Check_Box}
    Element Should be Enabled  ${Submit_Button}
    

Press Sign Up
    Click Element   ${Submit_Button}
    Wait Until Page Contains Element  ${Login_Submit}  60s
    
Check LogIn
    Input Text  ${Login_Username}  ${Work_Email_Manually_Filled}
    Input Text  ${Login_Password}  ${Password}
    Click Element  ${Login_Submit}
    Wait Until Page Contains  Dashboard  60s 
    ${Current_Url}=   Get Location 
    Should Be Equal  ${Current_Url}  https://${Broker_Tag_Automatically_Filled}.int.staging.insly.training/dashboard
    
    
***Test Cases***   
Insly Demo Workflow
    Launch Insly Demo Instance
    Insly Sign Up
    Enter Company Details
    Enter Administrator Account Details
    Tick All Terms and Conditions 
    Press Sign Up
    Check LogIn
    
     
